using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private float _timeBeforeClick;
    [SerializeField] private GameObject _preloaderCanvas;
    [SerializeField] private AudioSource _audioSource;

    public void PressPlayGame()
    {
        Invoke("PlayGame", _timeBeforeClick);
    }

    public void PressQuitGame()
    {
        Invoke("QuitGame", _timeBeforeClick);
    }

    private void PlayGame()
    {
        _audioSource.Play();
        _preloaderCanvas.SetActive(true);
        _preloaderCanvas.GetComponent<Preloader>().AsyncLoadLevelById(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void QuitGame()
    {
        _audioSource.Play();
        Application.Quit();
    }

    public void CancelPlayGame()
    {
        CancelInvoke("PlayGame");
    }

    public void CancelQuitGame()
    {
        CancelInvoke("QuitGame");
    }
}