﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;

public class GameManager : MonoBehaviour
{
    [Header("Physics")] [SerializeField] private float _shootingForce;

    [Space(5)] [Header("UI")] [SerializeField]
    private Button _mainMenuButton;

    [Space(5)] [Header("Spawns")] [SerializeField]
    private Transform _spawnPlayer;

    [Space(5)] [Header("Player")] [SerializeField]
    private GameObject _player;

    [Space(5)] [Header("Gun")] [SerializeField]
    private Transform _gunBody;

    [SerializeField] private Transform _gunPlatform;

    [Space(5)] [Header("Gun")] [SerializeField]
    private float _timeBeforeShot;

    [Space(5)] [Header("Gameplay")] [SerializeField]
    private int _livesDefault = 5;

    [SerializeField] private int _scorePrice;

    [Space(5)] [Header("Particles")] [SerializeField]
    private ParticleSystem _shotEffect;

    [Space(5)] [Header("Audio")] [SerializeField]
    private AudioSource _ambientAudio;

    private GameObject _clonePlayer;

    private bool _shot;

    private Rigidbody _playerRb;

    private int _lives;
    private int _score;

    private Text _timerText;
    private Text _timer;
    private Text _scoreText;

    public static GameManager Instance;

    private void Awake()
    {
        InputTracking.disablePositionalTracking = true;
        
    }


    private void Start()
    {
        if (Instance == null || Instance != this)
        {
            Instance = this;
        }

        _score = 0;
        Building.Instance.GenerationBricks();
        _lives = _livesDefault;
        Respawn();
    }

    private void Update()
    {
        if (_clonePlayer != null)
            RenderSettings.fog = _clonePlayer.transform.position.y < 0;
    }

    private void FixedUpdate()
    {
        if (_shot)
        {
            _playerRb.useGravity = true;
            _playerRb.AddForce(InputTracking.GetLocalRotation(XRNode.Head) * Vector3.forward * _shootingForce,
                ForceMode.Impulse);
            _shot = false;
        }
    }

    public void DestroyPlayer()
    {
        Destroy(_clonePlayer);
        _lives--;
        Respawn();
    }

    private void Respawn()
    {
        GunRestoreOriginState();
        RestoreLives();
        RespawnPlayer();

        _mainMenuButton.gameObject.SetActive(true);

        StartCoroutine(TimerBeforeShot());

        Invoke("Shot", _timeBeforeShot);
    }

    private void RespawnPlayer()
    {
        _clonePlayer = Instantiate(_player, _spawnPlayer.position, transform.rotation);

        _playerRb = _clonePlayer.GetComponent<Rigidbody>();

        GetUI();

        _scoreText.text = "Score: " + _score.ToString();
    }

    private void GunRestoreOriginState()
    {
        _gunBody.rotation = new Quaternion(0f, 0f, 0f, 0f);
        _gunPlatform.rotation = new Quaternion(0f, 0f, 0f, 0f);
    }


    private void RestoreLives()
    {
        if (_lives != 0)
            return;

        Building.Instance.RespawnBricks();
        _score = 0;
        _lives = _livesDefault;
    }

    public void CountScore()
    {
        _score += _scorePrice;
        _scoreText.text = "Score: " + _score.ToString();
    }

    private void Shot()
    {
        _clonePlayer.transform.SetParent(null);
        _clonePlayer.GetComponent<Animator>().enabled = false;

        _shot = true;

        _shotEffect.Play();
        _ambientAudio.Play();

        Invoke("DestroyPlayer", 20f);
    }

    private void GetUI()
    {
        _timerText = _clonePlayer.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>();
        _timer = _clonePlayer.transform.GetChild(0).GetChild(1).gameObject.GetComponent<Text>();
        _scoreText = _clonePlayer.transform.GetChild(0).GetChild(2).gameObject.GetComponent<Text>();
    }

    private IEnumerator TimerBeforeShot()
    {
        for (var i = _timeBeforeShot; i > 0; i--)
        {
            _timer.text = i.ToString();
            yield return new WaitForSeconds(1f);
        }

        _timer.gameObject.SetActive(false);
        _timerText.gameObject.SetActive(false);
        _mainMenuButton.gameObject.SetActive(false);
        _scoreText.gameObject.SetActive(false);
    }
}