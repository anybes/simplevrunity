﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour {
    [SerializeField] private float _timeBeforeClick;
    [SerializeField] private AudioSource _audioSource;
	
    public void PressMainMenu()
    {
        Invoke("MainMenu", _timeBeforeClick);
    }

    private void MainMenu()
    {
        _audioSource.Play();
        SceneManager.LoadScene(0);
    }

    public void CancelMainMenu()
    {
        CancelInvoke("MainMenu");
    }
}
