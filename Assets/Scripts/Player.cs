﻿using System;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private AudioClip _fallWaterClip;
    [SerializeField] private AudioClip _kickManClip;

    private bool _touched = false;

    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!_touched)
        {
            GameManager.Instance.CancelInvoke("DestroyPlayer");
            GameManager.Instance.Invoke("DestroyPlayer", 8f);
            _touched = true;
        }

     
            _audioSource.clip = other.transform.tag.Contains("Water") ? _fallWaterClip : _kickManClip;
            _audioSource.Play();
         
    }
}