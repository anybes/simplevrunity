﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Preloader : MonoBehaviour
{
    [SerializeField] private Text _progressText;
    private AsyncOperation _loadScene = null;

    private void OnGUI()
    {
        if (_loadScene != null)
        {
            _progressText.text = Mathf.RoundToInt(_loadScene.progress * 100) + "%";
        }
    }

    public void AsyncLoadLevelById(int levelID)
    {
        _loadScene = SceneManager.LoadSceneAsync(levelID);
    }
}