﻿using UnityEngine;
using UnityEngine.XR;

public class Gun : MonoBehaviour
{
    [SerializeField] private Transform _gun;
    [SerializeField] private Transform _platform;
    [SerializeField] private float _maxRotationX;
    [SerializeField] private float _minRotationX;
    [SerializeField] private float _maxRotationY;
    [SerializeField] private float _minRotationY;

    private void Awake()
    {
        InputTracking.disablePositionalTracking = true;
    }

    private void Update()
    {
        var xRotate = ClampAngle(InputTracking.GetLocalRotation(XRNode.Head).eulerAngles.x,
            _minRotationX, _maxRotationX);
        var yRotate = ClampAngle(InputTracking.GetLocalRotation(XRNode.Head).eulerAngles.y,
            _minRotationY, _maxRotationY);

        _gun.eulerAngles = new Vector3(Mathf.LerpAngle(_gun.eulerAngles.x, xRotate, 5f * Time.deltaTime),
            _gun.eulerAngles.y, _gun.eulerAngles.z);
        _platform.eulerAngles = new Vector3(_platform.eulerAngles.x,
            Mathf.LerpAngle(_platform.eulerAngles.y, yRotate, 5f * Time.deltaTime), _platform.eulerAngles.z);
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        angle = Mathf.Repeat(angle, 360);
        min = Mathf.Repeat(min, 360);
        max = Mathf.Repeat(max, 360);
        bool inverse = false;
        var tmin = min;
        var tangle = angle;
        if (min > 180)
        {
            inverse = !inverse;
            tmin -= 180;
        }

        if (angle > 180)
        {
            inverse = !inverse;
            tangle -= 180;
        }

        var result = !inverse ? tangle > tmin : tangle < tmin;
        if (!result)
            angle = min;

        inverse = false;
        tangle = angle;
        var tmax = max;
        if (angle > 180)
        {
            inverse = !inverse;
            tangle -= 180;
        }

        if (max > 180)
        {
            inverse = !inverse;
            tmax -= 180;
        }

        result = !inverse ? tangle < tmax : tangle > tmax;
        if (!result)
            angle = max;
        return angle;
    }
}