﻿using UnityEngine;
using Random = System.Random;

public class Island : MonoBehaviour
{
    [SerializeField] private ParticleSystem _smokePerticle;
    [SerializeField] private AudioClip[] _fallRockClips;

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag.Contains("Water") || !Building.Instance.Builded)
            return;

        Instantiate(_smokePerticle, other.transform.position, _smokePerticle.transform.rotation).Play();

        if (other.transform.tag.Contains("Brick"))
        {
            other.gameObject.GetComponent<AudioSource>().clip = GetFallRockAudioClip();
            other.gameObject.GetComponent<AudioSource>().Play();

            var brickObj = other.gameObject.GetComponent<Brick>();
            if (!brickObj.touch)
            {
                GameManager.Instance.CountScore();
                brickObj.touch = true;
            }
        }
    }

    private AudioClip GetFallRockAudioClip()
    {
        var maxIndex = _fallRockClips.Length - 1;
        var index = new Random().Next(0, maxIndex);
        return _fallRockClips[index];
    }
}