﻿using UnityEngine;

public class Building : MonoBehaviour
{
    [SerializeField] private GameObject _buildPrefab;
    [SerializeField] private Transform _buildSpawnPoint;

    private GameObject _cloneBuild;
    
    public bool Builded;
    public static Building Instance;

    private void Awake()
    {
        if (Instance == null || Instance != this)
        {
            Instance = this;
        }
    }

    public void GenerationBricks()
    {
        _cloneBuild = Instantiate(_buildPrefab, _buildSpawnPoint.position, _buildSpawnPoint.rotation);
        Invoke("BuildFinished", 1f);
    }

    private void BuildFinished()
    {
        Builded = true;
    }

    public void RespawnBricks()
    {
        Builded = false;
        Destroy(_cloneBuild);
        GenerationBricks();
    }
}